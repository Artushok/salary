FROM python:3.12

RUN pip install poetry

RUN poetry config virtualenvs.create false

WORKDIR /app/

COPY ["pyproject.toml", "poetry.lock", "/app/"]

RUN poetry install --only main

ENV POSTGRES_USER=postgres

ENV POSTGRES_PASSWORD=postgres

ENV POSTGRES_HOST=localhost

ENV POSTGRES_PORT=5432

ENV POSTGRES_DB=postgres

ENV SECRET_KEY=secret

ENV ALGORITHM=HS256

ENV TOKEN_EXPIRE_MINUTES=10

ENV FIRST_SUPERUSER=admin

ENV FIRST_SUPERUSER_PASSWORD=123456

COPY alembic.ini /app/

COPY /alembic/ /app/alembic/

COPY /srs/ /app/srs/

WORKDIR /app/srs/

RUN chmod +x ./app.sh

CMD ./app.sh