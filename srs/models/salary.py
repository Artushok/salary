from datetime import datetime, date

from sqlalchemy import ForeignKey, text
from sqlalchemy.orm import Mapped, mapped_column, relationship, backref

from models.base import Base


class SalaryOrm(Base):
    __tablename__ = "salary"

    id: Mapped[int] = mapped_column(primary_key=True)
    employe_id: Mapped[int] = mapped_column(
        ForeignKey("employe.id", ondelete="CASCADE")
    )
    created_at: Mapped[datetime] = mapped_column(
        server_default=text("TIMEZONE('utc', now())")
    )
    salary: Mapped[float]
    date_of_increase: Mapped[date]

    employe: Mapped["EmployeOrm"] = relationship(
        "EmployeOrm", backref=backref("salaries", cascade="all, delete-orphan")
    )
