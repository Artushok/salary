from sqlalchemy import Boolean
from sqlalchemy.orm import Mapped, mapped_column

from models.base import Base


class EmployeOrm(Base):
    __tablename__ = "employe"

    id: Mapped[int] = mapped_column(primary_key=True)
    username: Mapped[str] = mapped_column(unique=True)
    hashed_password: Mapped[bytes]
    is_admin: Mapped[bool] = mapped_column(Boolean, default=False)
