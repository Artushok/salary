from datetime import date, datetime
from pydantic import BaseModel, ConfigDict


class TokenResponse(BaseModel):
    access_token: str
    token_type: str = "Bearer"


class EmployeBase(BaseModel):
    pass


class EmployeUpdatePassword(EmployeBase):
    password: str
    new_password: str


class EmployeWithSalaryCreate(EmployeBase):
    username: str
    password: str
    is_admin: bool = False
    salary: float
    date_of_increase: date


class EmployeResponse(EmployeBase):
    model_config = ConfigDict(from_attributes=True)

    id: int
    username: str
    is_admin: bool = False


class SalaryBase(BaseModel):
    employe_id: int


class SalaryCreate(SalaryBase):
    salary: float
    date_of_increase: date


class SalaryResponse(SalaryCreate):
    model_config = ConfigDict(from_attributes=True)

    id: int
    created_at: datetime
