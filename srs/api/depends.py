from typing import Annotated

from fastapi import Depends, Form, HTTPException, status
from fastapi.security import OAuth2PasswordBearer, HTTPAuthorizationCredentials
from sqlalchemy.ext.asyncio import AsyncSession
import crud
import jwt
from pydantic import ValidationError

from database import get_async_session
from config import settings
from models.employe import EmployeOrm
from utils import verify_password


SessionDep = Annotated[AsyncSession, Depends(get_async_session)]


async def validate_auth_user(
    session: SessionDep,
    username: str = Form(),
    password: str = Form(),
):
    user = await crud.get_employe_by_username(session, username)
    if not user:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Invalid username or password",
        )

    if not verify_password(
        password=password,
        hashed_password=user.hashed_password,
    ):
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Invalid username or password",
        )
    return user


ValidateUserDep = Annotated[EmployeOrm, Depends(validate_auth_user)]

oauth2_scheme = OAuth2PasswordBearer(
    tokenUrl="auth/token",
)

TokenDep = Annotated[HTTPAuthorizationCredentials, Depends(oauth2_scheme)]


async def get_current_user(session: SessionDep, token: TokenDep) -> EmployeOrm:
    try:
        payload = jwt.decode(
            token, settings.SECRET_KEY, algorithms=[settings.ALGORITHM]
        )
    except (jwt.InvalidTokenError, ValidationError):
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Token invalid",
        )
    user = await session.get(EmployeOrm, payload["sub"])
    if not user:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED, detail="Token invalid"
        )
    return user


CurrentEmployeDep = Annotated[EmployeOrm, Depends(get_current_user)]


def get_current_admin(current_employe: CurrentEmployeDep) -> EmployeOrm:
    if not current_employe.is_admin:
        raise HTTPException(
            status_code=403, detail="The user doesn't have enough privileges"
        )
    return current_employe


CurrentAdminDep = Annotated[EmployeOrm, Depends(get_current_admin)]
