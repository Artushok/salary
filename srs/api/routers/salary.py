from typing import List
from fastapi import APIRouter, HTTPException, status

from api.schemas import SalaryCreate, SalaryResponse
import crud
from models.salary import SalaryOrm
from api.depends import SessionDep, CurrentEmployeDep, CurrentAdminDep


salaryRouter = APIRouter()


@salaryRouter.get("/", response_model=SalaryResponse)
async def get_curent_salary(session: SessionDep, user: CurrentEmployeDep):
    res = await crud.get_curent_salary(session, user.id)
    if not res:
        raise HTTPException(status.HTTP_404_NOT_FOUND)
    result = SalaryResponse.model_validate(res)
    return result


@salaryRouter.get("/history/{id}", response_model=List[SalaryResponse])
async def get_salery_history(id: int, session: SessionDep, admin: CurrentAdminDep):
    employe = await crud.get_employe_by_id(session, id)
    if not employe:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND, detail="User not found"
        )
    res = await crud.get_salary_history(session, id)
    result = [SalaryResponse.model_validate(i) for i in res]
    return result


@salaryRouter.get("/history/", response_model=List[SalaryResponse])
async def get_salery_history(session: SessionDep, user: CurrentEmployeDep):
    res = await crud.get_salary_history(session, user.id)
    result = [SalaryResponse.model_validate(i) for i in res]
    return result


@salaryRouter.post(
    "/", response_model=SalaryResponse, status_code=201, response_description="Created"
)
async def create_salery(
    salary: SalaryCreate, session: SessionDep, admin: CurrentAdminDep
):
    employe = await crud.get_employe_by_id(session, SalaryCreate.employe_id)
    if not employe:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND, detail="User not found"
        )
    data = SalaryOrm(
        employe_id=salary.employe_id,
        salary=salary.salary,
        date_of_increase=salary.date_of_increase,
    )
    res = await crud.insert_salary(session, data)
    result = SalaryResponse.model_validate(res)
    return result


@salaryRouter.delete("/{id}")
async def delete_salary(id: int, session: SessionDep, admin: CurrentAdminDep):
    employe = await session.get(SalaryOrm, id)
    if not employe:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND, detail="Salary not found"
        )
    await crud.delete_salary(session, id)
    return "Salary deleted successfully"
