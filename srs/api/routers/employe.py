from typing import List
from fastapi import APIRouter, HTTPException, status

import crud
from models.employe import EmployeOrm
from models.salary import SalaryOrm
from api.schemas import EmployeResponse, EmployeWithSalaryCreate
from api.depends import SessionDep, CurrentAdminDep
from utils import get_password_hash


employeRouter = APIRouter()


@employeRouter.get("/", response_model=List[EmployeResponse])
async def get_employees(session: SessionDep, admin: CurrentAdminDep):
    res = await crud.get_all_employees(session)
    employees = [EmployeResponse.model_validate(i) for i in res]
    return employees


@employeRouter.post(
    "/", response_model=EmployeResponse, status_code=201, response_description="Created"
)
async def create_employe_with_salary(
    data: EmployeWithSalaryCreate,
    session: SessionDep,
    admin: CurrentAdminDep,
):
    res = await crud.get_employe_by_username(session, data.username)
    if res:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST, detail="User already exist"
        )
    employe = EmployeOrm(
        username=data.username,
        hashed_password=get_password_hash(data.password),
        is_admin=data.is_admin,
    )
    salary = SalaryOrm(salary=data.salary, date_of_increase=data.date_of_increase)
    res = await crud.insert_employe(session, employe, salary)
    result = EmployeResponse.model_validate(res)
    return result


@employeRouter.delete("/{id}")
async def delete_employe(id: int, session: SessionDep, admin: CurrentAdminDep):
    employe = await crud.get_employe_by_id(session, id)
    if not employe:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND, detail="User not found"
        )
    await crud.delete_employe(session, employe)
    return "User deleted successfully"


@employeRouter.patch("/{id}/role", response_model=EmployeResponse)
async def change_role_for_employe(
    id: int, is_admin: bool, session: SessionDep, admin: CurrentAdminDep
):
    employe = await crud.get_employe_by_id(session, id)
    if not employe:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND, detail="User not found"
        )
    res = await crud.change_role(session, employe, is_admin)
    result = EmployeResponse.model_validate(res)
    return result
