from fastapi import APIRouter, HTTPException, status

import crud
from api.depends import CurrentEmployeDep, SessionDep, ValidateUserDep
from api.schemas import EmployeUpdatePassword, TokenResponse
from utils import create_access_token, get_password_hash, verify_password


authRouter = APIRouter()


@authRouter.post("/token", response_model=TokenResponse)
async def get_token(employe: ValidateUserDep):
    payload = {
        "sub": employe.id,
    }
    token = create_access_token(payload)
    return TokenResponse(access_token=token)


@authRouter.patch("/password")
async def change_password(
    session: SessionDep, data: EmployeUpdatePassword, user: CurrentEmployeDep
):
    if not verify_password(data.password, user.hashed_password):
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Invalid username or password",
        )
    hash_password = get_password_hash(data.new_password)
    await crud.change_password(session, user, hash_password)
    return "Password changed successfully"
