#!/bin/bash

cd ..
alembic upgrade head
cd srs/
python superuser.py
uvicorn main:app --host 0.0.0.0 --port 80
