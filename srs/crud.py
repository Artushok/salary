from typing import List
from sqlalchemy import and_, select, desc, func

from sqlalchemy.ext.asyncio import AsyncSession

from models import EmployeOrm, SalaryOrm


async def insert_employe(
    session: AsyncSession, employe: EmployeOrm, salary: SalaryOrm
) -> EmployeOrm:
    employe.salaries = [salary]
    session.add(employe)
    await session.commit()
    return employe


async def get_all_employees(session: AsyncSession) -> List[EmployeOrm]:
    stmt = select(EmployeOrm)
    res = await session.execute(stmt)
    employees = res.scalars().all()
    return employees


async def get_employe_by_id(session: AsyncSession, id: int) -> EmployeOrm | None:
    employe = await session.get(EmployeOrm, id)
    return employe


async def get_employe_by_username(
    session: AsyncSession, username: str
) -> EmployeOrm | None:
    stmt = select(EmployeOrm).where(EmployeOrm.username == username)
    res = await session.execute(stmt)
    employe = res.scalar()
    return employe


async def change_password(
    session: AsyncSession, employe: EmployeOrm, new_hashed_password: str
) -> None:
    employe.hashed_password = new_hashed_password
    await session.commit()


async def change_role(
    session: AsyncSession, employe: EmployeOrm, is_admin: bool
) -> EmployeOrm:
    employe.is_admin = is_admin
    await session.commit()
    return employe


async def delete_employe(session: AsyncSession, employe: EmployeOrm) -> None:
    await session.delete(employe)
    await session.commit()


async def insert_salary(session: AsyncSession, salary: SalaryOrm) -> SalaryOrm:
    session.add(salary)
    await session.commit()
    return salary


async def get_curent_salary(session: AsyncSession, employe_id: int) -> SalaryOrm | None:
    subquery = (
        select(func.max(SalaryOrm.created_at))
        .where(SalaryOrm.employe_id == employe_id)
        .group_by(SalaryOrm.employe_id)
    )
    stmt = select(SalaryOrm).where(
        and_(SalaryOrm.employe_id == employe_id, SalaryOrm.created_at == subquery)
    )
    res = await session.execute(stmt)
    salary = res.scalars().one_or_none()
    return salary


async def get_salary_history(session: AsyncSession, employe_id: int) -> List[SalaryOrm]:
    stmt = (
        select(SalaryOrm)
        .where(SalaryOrm.employe_id == employe_id)
        .order_by(desc(SalaryOrm.created_at))
    )
    res = await session.execute(stmt)
    salary = res.scalars().all()
    print(salary)
    return salary


async def delete_salary(session: AsyncSession, salary: SalaryOrm) -> None:
    await session.delete(salary)
    await session.commit()
