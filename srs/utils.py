from datetime import datetime, timedelta, UTC

import bcrypt
import jwt


from config import settings


def get_password_hash(password: str) -> bytes:
    salt = bcrypt.gensalt()
    pwd_bytes = password.encode()
    return bcrypt.hashpw(pwd_bytes, salt)


def verify_password(password: str, hashed_password: bytes) -> bool:
    return bcrypt.checkpw(password.encode(), hashed_password)


def create_access_token(payload: dict) -> str:
    to_encode = payload.copy()
    expire = datetime.now(UTC) + timedelta(minutes=settings.TOKEN_EXPIRE_MINUTES)
    to_encode.update(
        exp=expire,
    )
    encoded_jwt = jwt.encode(
        to_encode, settings.SECRET_KEY, algorithm=settings.ALGORITHM
    )

    return encoded_jwt
