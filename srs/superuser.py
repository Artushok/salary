import asyncio
from sqlalchemy.ext.asyncio import AsyncSession, create_async_engine

from crud import get_employe_by_username
from config import settings
from models.employe import EmployeOrm
from utils import get_password_hash

engine = create_async_engine(url=settings.DB_URL, echo=False)
session = AsyncSession(engine)


async def create_superuser(session: AsyncSession) -> None:
    user = await get_employe_by_username(session, settings.FIRST_SUPERUSER)
    if not user:
        user = EmployeOrm(
            username=settings.FIRST_SUPERUSER,
            hashed_password=get_password_hash(settings.FIRST_SUPERUSER_PASSWORD),
            is_admin=True,
        )
        session.add(user)
        await session.commit()


asyncio.run(create_superuser(session))
