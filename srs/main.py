from fastapi import FastAPI
from api.routers.employe import employeRouter
from api.routers.auth import authRouter
from api.routers.salary import salaryRouter

import uvicorn


app = FastAPI(title="Salary")
app.include_router(authRouter, prefix="/auth", tags=["Authentication"])
app.include_router(employeRouter, prefix="/employees", tags=["Employe"])
app.include_router(salaryRouter, prefix="/salary", tags=["Salary"])


if __name__ == "__main__":
    uvicorn.run("main:app", reload=True)
